#!/bin/bash

set -e

BOARD_DIR="$(dirname $0)"
BOARD_NAME="$(basename ${BOARD_DIR})"
GENIMAGE_CFG="${BOARD_DIR}/genimage-raspberrypi3.cfg"
GENIMAGE_TMP="${BUILD_DIR}/genimage.tmp"

for arg in "$@"
do
	case "${arg}" in
		--add-pi3-miniuart-bt-overlay)
		if ! grep -qE '^dtoverlay=' "${BINARIES_DIR}/rpi-firmware/config.txt"; then
			echo "Adding 'dtoverlay=pi3-miniuart-bt' to config.txt (fixes ttyAMA0 serial console)."
			cat << __EOF__ >> "${BINARIES_DIR}/rpi-firmware/config.txt"

# fixes rpi3 ttyAMA0 serial console
dtoverlay=miniuart-bt
__EOF__
		fi
		;;
		--aarch64)
		# Run a 64bits kernel (armv8)
		sed -e '/^kernel=/s,=.*,=Image,' -i "${BINARIES_DIR}/rpi-firmware/config.txt"
		if ! grep -qE '^arm_64bit=1' "${BINARIES_DIR}/rpi-firmware/config.txt"; then
			cat << __EOF__ >> "${BINARIES_DIR}/rpi-firmware/config.txt"

# enable 64bits support
arm_64bit=1
__EOF__
		fi

		# Enable uart console
		if ! grep -qE '^enable_uart=1' "${BINARIES_DIR}/rpi-firmware/config.txt"; then
			cat << __EOF__ >> "${BINARIES_DIR}/rpi-firmware/config.txt"

# enable rpi3 ttyS0 serial console
enable_uart=1
__EOF__
		fi
		;;
		--gpu_mem_256=*|--gpu_mem_512=*|--gpu_mem_1024=*)
		# Set GPU memory
		gpu_mem="${arg:2}"
		sed -e "/^${gpu_mem%=*}=/s,=.*,=${gpu_mem##*=}," -i "${BINARIES_DIR}/rpi-firmware/config.txt"
		;;
	esac

done

cat << __EOF__ >> "${BINARIES_DIR}/rpi-firmware/config.txt"

# Hifiberry DAC+ ADC Pro for LTC reception
dtoverlay=hifiberry-dacplusadcpro
__EOF__


cp "${TARGET_DIR}/hyperpixel4_square/hyperpixel4-s.dtbo" "${BINARIES_DIR}/rpi-firmware/overlays/"
cp "${TARGET_DIR}/hyperpixel4_square/config.txt" "${BINARIES_DIR}/config.txt.hp4_square"
cp "${TARGET_DIR}/hyperpixel4_rect/hyperpixel4-r.dtbo" "${BINARIES_DIR}/rpi-firmware/overlays/"
cp "${TARGET_DIR}/hyperpixel4_rect/config.txt" "${BINARIES_DIR}/config.txt.hp4_rect"
cp "${TARGET_DIR}/hyperpixel4_rect/hyperpixel4-init" "${BINARIES_DIR}/hyperpixel4-init"
cp "${TARGET_DIR}/etc/network/interfaces" "${BINARIES_DIR}/interfaces"
cp "${TARGET_DIR}/etc/ntp.conf" "${BINARIES_DIR}/ntp.conf"
cp "${TARGET_DIR}/root/clock_cmd.sh" "${BINARIES_DIR}/clock_cmd.sh"
cp "${TARGET_DIR}/root/clock_bridge_cmd.sh" "${BINARIES_DIR}/clock_bridge_cmd.sh"
cp "${TARGET_DIR}/root/alsa-ltc_cmd.sh" "${BINARIES_DIR}/alsa-ltc_cmd.sh"
cp "${TARGET_DIR}/root/clock.ini" "${BINARIES_DIR}/clock.ini"

rm -fr "${BUILD_DIR}/clock-8001_git"
git clone --depth 1 https://gitlab.com/clock-8001/clock-8001.git "${BUILD_DIR}/clock-8001_git"
cp -r "${BUILD_DIR}/clock-8001_git/v4/voices" "${BINARIES_DIR}/"

touch "${BINARIES_DIR}/enable_clock"
touch "${BINARIES_DIR}/enable_bridge"
touch "${BINARIES_DIR}/enable_ssh"
touch "${BINARIES_DIR}/enable_ltc"

rm -rf "${GENIMAGE_TMP}"

genimage                           \
	--rootpath "${TARGET_DIR}"     \
	--tmppath "${GENIMAGE_TMP}"    \
	--inputpath "${BINARIES_DIR}"  \
	--outputpath "${BINARIES_DIR}" \
	--config "${GENIMAGE_CFG}"

exit $?
