################################################################################
#
# Led matrix clock
#
################################################################################

SDL_CLOCK_VERSION = 4.0
SDL_CLOCK_SITE_METHOD = local
SDL_CLOCK_SITE = $(BR2_EXTERNAL_CLOCK_8001_PATH)/package/sdl_clock

define SDL_CLOCK_BUILD_CMDS
	wget https://gitlab.com/clock-8001/clock-8001/-/jobs/artifacts/master/raw/v4/sdl-clock?job=build_v4 -O $(@D)/sdl-clock
	wget https://gitlab.com/clock-8001/clock-8001/-/jobs/artifacts/master/raw/v4/clock_port80.ini?job=create_config_4 -O $(@D)/clock.ini

	wget https://gitlab.com/clock-8001/clock-8001/-/raw/master/v4/ttf_fonts/Copse-Regular.ttf 					-O $(@D)/Copse-Regular.ttf
	wget https://gitlab.com/clock-8001/clock-8001/-/raw/master/v4/ttf_fonts/DejaVuSans.ttf 						-O $(@D)/DejaVuSans.ttf
	wget https://gitlab.com/clock-8001/clock-8001/-/raw/master/v4/ttf_fonts/FiraCode-VariableFont_wght.ttf 		-O $(@D)/FiraCode-VariableFont_wght.ttf
	wget https://gitlab.com/clock-8001/clock-8001/-/raw/master/v4/ttf_fonts/Lato-Black.ttf 						-O $(@D)/Lato-Black.ttf
	wget https://gitlab.com/clock-8001/clock-8001/-/raw/master/v4/ttf_fonts/Lato-Bold.ttf 						-O $(@D)/Lato-Bold.ttf
	wget https://gitlab.com/clock-8001/clock-8001/-/raw/master/v4/ttf_fonts/Lato-Light.ttf 						-O $(@D)/Lato-Light.ttf
	wget https://gitlab.com/clock-8001/clock-8001/-/raw/master/v4/ttf_fonts/Lato-Regular.ttf 					-O $(@D)/Lato-Regular.ttf
	wget https://gitlab.com/clock-8001/clock-8001/-/raw/master/v4/ttf_fonts/Lato-Thin.ttf 						-O $(@D)/Lato-Thin.ttf
	wget https://gitlab.com/clock-8001/clock-8001/-/raw/master/v4/ttf_fonts/MajorMonoDisplay-Regular.ttf 		-O $(@D)/MajorMonoDisplay-Regular.ttf
	wget https://gitlab.com/clock-8001/clock-8001/-/raw/master/v4/ttf_fonts/MaterialIcons-Regular.ttf 			-O $(@D)/MaterialIcons-Regular.ttf
	wget https://gitlab.com/clock-8001/clock-8001/-/raw/master/v4/ttf_fonts/OxygenMono-Regular.ttf 				-O $(@D)/OxygenMono-Regular.ttf
	wget https://gitlab.com/clock-8001/clock-8001/-/raw/master/v4/ttf_fonts/RobotoMono-VariableFont_wght.ttf 	-O $(@D)/RobotoMono-VariableFont_wght.ttf
	wget https://gitlab.com/clock-8001/clock-8001/-/raw/master/v4/ttf_fonts/Ruluko-Regular.ttf 					-O $(@D)/Ruluko-Regular.ttf
	wget https://gitlab.com/clock-8001/clock-8001/-/raw/master/v4/ttf_fonts/VarelaRound-Regular.ttf 			-O $(@D)/VarelaRound-Regular.ttf
endef

define SDL_CLOCK_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/root/fonts
	$(INSTALL) -D -m 0755 $(@D)/sdl-clock $(TARGET_DIR)/root/sdl-clock
	$(INSTALL) -D -m 0755 $(@D)/clock.ini $(TARGET_DIR)/root/clock.ini
	$(INSTALL) -D -m 0755 $(@D)/rpi-eth-util $(TARGET_DIR)/root/rpi-eth-util
	$(INSTALL) -D -m 0755 $(@D)/7x13.bdf $(TARGET_DIR)/root/fonts/7x13.bdf

	# fonts
	$(INSTALL) -D -m 0755 $(@D)/*.ttf $(TARGET_DIR)/root/

	# init scripts
	$(INSTALL) -D -m 0755 $(@D)/S03copy_clock_files $(TARGET_DIR)/etc/init.d/S03copy_clock_files
	# $(INSTALL) -D -m 0755 $(@D)/S03usb_serial $(TARGET_DIR)/etc/init.d/S03usb_serial
	$(INSTALL) -D -m 0755 $(@D)/S02wait_eth0 $(TARGET_DIR)/etc/init.d/S02wait_eth0
	$(INSTALL) -D -m 0755 $(@D)/S99clock $(TARGET_DIR)/etc/init.d/S99clock

	$(INSTALL) -D -m 0755 $(@D)/clock_pokemon.sh $(TARGET_DIR)/root/clock_pokemon.sh
	echo "/root/sdl-clock $(BR2_PACKAGE_SDL_CLOCK_CONFIG)" > $(@D)/clock_cmd.sh
	$(INSTALL) -D -m 0755 $(@D)/clock_cmd.sh $(TARGET_DIR)/root/clock_cmd.sh
endef

$(eval $(generic-package))
