################################################################################
#
# Led matrix clock
#
################################################################################

CLOCK_BRIDGE_VERSION = 3.0
CLOCK_BRIDGE_SITE_METHOD = local
CLOCK_BRIDGE_SITE = $(BR2_EXTERNAL_CLOCK_8001_PATH)/package/clock_bridge

define CLOCK_BRIDGE_BUILD_CMDS
	wget https://gitlab.com/clock-8001/clock-8001/-/jobs/artifacts/master/raw/v3/clock-bridge?job=build -O $(@D)/clock-bridge
endef

define CLOCK_BRIDGE_INSTALL_TARGET_CMDS
	$(INSTALL) -D -m 0755 $(@D)/clock-bridge $(TARGET_DIR)/root/clock-bridge
	$(INSTALL) -D -m 0755 $(@D)/rpi-eth-util $(TARGET_DIR)/root/rpi-eth-util

	# init scripts
	$(INSTALL) -D -m 0755 $(@D)/S03copy_clock_bridge_files $(TARGET_DIR)/etc/init.d/S03copy_clock_bridge_files
	$(INSTALL) -D -m 0755 $(@D)/S02wait_eth0 $(TARGET_DIR)/etc/init.d/S02wait_eth0
	$(INSTALL) -D -m 0755 $(@D)/S99clock_bridge $(TARGET_DIR)/etc/init.d/S99clock_bridge

	$(INSTALL) -D -m 0755 $(@D)/clock_bridge_pokemon.sh $(TARGET_DIR)/root/clock_bridge_pokemon.sh
	echo "/root/clock-bridge $(BR2_PACKAGE_CLOCK_BRIDGE_CONFIG)" > $(@D)/clock_bridge_cmd.sh
	$(INSTALL) -D -m 0755 $(@D)/clock_bridge_cmd.sh $(TARGET_DIR)/root/clock_bridge_cmd.sh
endef

$(eval $(generic-package))
